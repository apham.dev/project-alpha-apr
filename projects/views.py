from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from tasks.models import Task
from projects.forms import ProjectForm
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required
def list_projects(request):
    all_the_lists = Project.objects.filter(owner=request.user)
    context = {"all_the_lists": all_the_lists}
    return render(request, "projects/list_projects.html", context)


@login_required
def show_project(request, id):
    one_project = get_object_or_404(Project, id=id)
    task_attributes = Task.objects.filter(id=id)
    list_of_tasks = Task.objects.filter(project=id)
    context = {
        "one_project": one_project,
        "task_attributes": task_attributes,
        "list_of_tasks": list_of_tasks,
    }
    return render(request, "projects/show_project.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.owner = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create_project.html", context)
